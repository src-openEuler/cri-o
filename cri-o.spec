%global goipath         github.com/cri-o/cri-o
%define gobuild(o:) %{expand:
  %global _dwz_low_mem_die_limit 0
  %ifnarch ppc64
  go build -buildmode pie -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${BASE_LDFLAGS:-}%{?currentgoldflags} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags %{?__golang_extldflags}' -compressdwarf=false" -a -v -x %{?**};
  %else
  go build                -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${BASE_LDFLAGS:-}%{?currentgoldflags} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags %{?__golang_extldflags}' -compressdwarf=false" -a -v -x %{?**};
  %endif
}
%bcond_with check

%global build_timestamp %(date -u +'%Y-%m-%dT%H:%M:%SZ')
%global git_tree_state clean
%global criocli_path ""
%global built_tag v%{version}
%global built_tag_strip %(b=%{built_tag}; echo ${b:1})
%global crio_release_tag %(echo %{built_tag_strip} | cut -f1,2 -d'.')
%global service_name crio
%global commit0 d317b5dc918bbfbc78481072a0d93e572aa8d0e8

Name:           cri-o
Version:        1.29.2
Epoch:          0
Release:        5
Summary:        Open Container Initiative-based implementation of Kubernetes Container Runtime Interface
License:        ASL 2.0
URL:            https://github.com/cri-o/cri-o
Source0:        https://github.com/cri-o/cri-o/archive/refs/tags/v%{version}.tar.gz
Source1:        https://github.com/cpuguy83/go-md2man/archive/refs/tags/v2.0.3.tar.gz

Patch0001:      0001-fix-CVE-2024-24786.patch
Patch0002:      0002-fix-CVE-2023-48795.patch
Patch0003:      0003-fix-CVE-2024-28180.patch

ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 aarch64 %{arm} riscv64}
BuildRequires:  btrfs-progs-devel device-mapper-devel go-srpm-macros
BuildRequires:  golang >= 1.21, git-core, glib2-devel, glibc-static, openEuler-rpm-config
BuildRequires:  gpgme-devel, libassuan-devel, libseccomp-devel, systemd-devel, make
Requires:       container-selinux, containers-common >= 1:0.1.31-14, runc >= 1.0.0-16
Requires:       containernetworking-plugins >= 1.0.0-1, conmon >= 2.0.2-1, socat
Obsoletes:      ocid <= 0.3
Provides:       ocid = %{epoch}:%{version}-%{release}
Provides:       %{service_name} = %{epoch}:%{version}-%{release}

%description
Open Container Initiative-based implementation of Kubernetes Container Runtime
Interface.

%prep
%autosetup -p1 -n %{name}-%{version}
tar -xf %SOURCE1
sed -i 's/install.config: crio.conf/install.config:/' Makefile
sed -i 's/install.bin: binaries/install.bin:/' Makefile
sed -i 's/install.man: $(MANPAGES)/install.man:/' Makefile
sed -i 's/\.gopathok //' Makefile
sed -i 's/module_/module-/' internal/version/version.go
sed -i 's/\/local//' contrib/systemd/%{service_name}.service
sed -i 's/\/local//' contrib/systemd/%{service_name}-wipe.service

%build
GO_MD2MAN_PATH="$(pwd)%{_bindir}"
mkdir -p _build/bin $GO_MD2MAN_PATH
cd go-md2man-*
go build -mod=vendor -o ../_build/bin/go-md2man .
cp ../_build/bin/go-md2man $GO_MD2MAN_PATH/go-md2man
export PATH=$GO_MD2MAN_PATH:$PATH
cd -

export GO111MODULE=on
export GOFLAGS=-mod=vendor

export BUILDTAGS="$(hack/btrfs_installed_tag.sh)
$(hack/btrfs_tag.sh) $(hack/libdm_installed.sh)
$(hack/libdm_no_deferred_remove_tag.sh)
$(hack/seccomp_tag.sh)
$(hack/selinux_tag.sh)"

export BASE_LDFLAGS="-X %{goipath}/internal/pkg/criocli.DefaultsPath=%{criocli_path}
-X  %{goipath}/internal/version.buildDate=%{build_timestamp}
-X  %{goipath}/internal/version.gitCommit=%{commit0}
-X  %{goipath}/internal/version.version=%{version}
-X  %{goipath}/internal/version.gitTreeState=%{git_tree_state} "

for cmd in cmd/* ; do
  %gobuild -o bin/$(basename $cmd) %{goipath}/$cmd
done

export CFLAGS="$CFLAGS -std=c99"
%make_build bin/pinns
GO_MD2MAN=go-md2man make docs

%install
sed -i 's/\/local//' contrib/systemd/%{service_name}.service
bin/%{service_name} \
      --selinux \
      --cni-plugin-dir /opt/cni/bin \
      --cni-plugin-dir "%{_libexecdir}/cni" \
      --enable-metrics \
      --metrics-port 9537 \
      config > %{service_name}.conf

# install binaries
install -dp %{buildroot}{%{_bindir},%{_libexecdir}/%{service_name}}
install -p -m 755 bin/%{service_name} %{buildroot}%{_bindir}

# install conf files
install -dp %{buildroot}%{_sysconfdir}/cni/net.d
install -p -m 644 contrib/cni/10-crio-bridge.conflist %{buildroot}%{_sysconfdir}/cni/net.d/100-crio-bridge.conflist
install -p -m 644 contrib/cni/99-loopback.conflist %{buildroot}%{_sysconfdir}/cni/net.d/200-loopback.conflist

install -dp %{buildroot}%{_sysconfdir}/%{service_name}
install -dp %{buildroot}%{_datadir}/containers/oci/hooks.d
install -dp %{buildroot}%{_datadir}/oci-umount/oci-umount.d
install -p -m 644 crio.conf %{buildroot}%{_sysconfdir}/%{service_name}
install -p -m 644 crio-umount.conf %{buildroot}%{_datadir}/oci-umount/oci-umount.d/%{service_name}-umount.conf
install -p -m 644 crictl.yaml %{buildroot}%{_sysconfdir}

%make_install PREFIX=%{buildroot}%{_prefix} \
            install.bin \
            install.completions \
            install.config \
            install.man \
            install.systemd

install -dp %{buildroot}%{_sharedstatedir}/containers

%post
%systemd_post %{service_name}

%preun
%systemd_preun %{service_name}

%postun
%systemd_postun_with_restart %{service_name}

%files
%license LICENSE
%doc docs code-of-conduct.md tutorial.md ADOPTERS.md CONTRIBUTING.md README.md
%doc awesome.md transfer.md
%{_bindir}/%{service_name}
%{_bindir}/pinns
%{_mandir}/man5/%{service_name}.conf*5*
%{_mandir}/man8/%{service_name}*.8*
%dir %{_sysconfdir}/%{service_name}
%config(noreplace) %{_sysconfdir}/%{service_name}/%{service_name}.conf
%config(noreplace) %{_sysconfdir}/cni/net.d/100-%{service_name}-bridge.conflist
%config(noreplace) %{_sysconfdir}/cni/net.d/200-loopback.conflist
%config(noreplace) %{_sysconfdir}/crictl.yaml
%dir %{_libexecdir}/%{service_name}
%{_unitdir}/%{service_name}.service
%{_unitdir}/%{service_name}-wipe.service
%dir %{_sharedstatedir}/containers
%dir %{_datadir}/containers
%dir %{_datadir}/containers/oci
%dir %{_datadir}/containers/oci/hooks.d
%dir %{_datadir}/oci-umount
%dir %{_datadir}/oci-umount/oci-umount.d
%{_datadir}/oci-umount/oci-umount.d/%{service_name}-umount.conf
%{_datadir}/bash-completion/completions/%{service_name}*
%{_datadir}/fish/completions/%{service_name}*.fish
%{_datadir}/zsh/site-functions/_%{service_name}*

%changelog
* Fri May 24 2024 Jingwiw <wangjingwei@iscas.ac.cn> - 0:1.29.2-5
- Type:enhancement
- CVE:NA
- SUG:NA
- DESC: enable riscv64

* Thu Mar 28 2024 zhangbowei <zhangbowei@kylinos.cn> - 0:1.29.2-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: CVE-2024-28180

* Tue Mar 26 2024 zhangbowei <zhangbowei@kylinos.cn> - 0:1.29.2-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2023-48795

* Mon Mar 25 2024 zhangbowei <zhangbowei@kylinos.cn> - 0:1.29.2-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-24786

* Wed Feb 28 2024 chendexi <chendexi@kylinos.cn> - 0:1.29.2-1
- Update cri-o to 1.29.2

* Tue Jun 07 2022 fushanqing <fushanqing@kylinos.cn> - 0:1.23.2-1
- Update cri-o to 1.23.2

* Mon Mar 21 2022 fushanqing <fushanqing@kylinos.cn> - 0:1.22.1-1
- Init Package

